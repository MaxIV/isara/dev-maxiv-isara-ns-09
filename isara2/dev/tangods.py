"""Tango device server."""

from functools import (
    partial,
    wraps,
)

from tango import (
    AttrDataFormat,
    AttrWriteType,
    CmdArgType,
    DevState,
    Except,
)
from tango.server import (
    Device,
    attribute,
    command,
    device_property,
)

from isara2.client import (
    CommunicationError,
    IsaraBaseConnectionClient,
    IsaraException,
    connect,
)
from isara2.client.errors import NoAttributeWithName


# for handling exceptions that might occur in the function
def handle_error(func=None, msg="Error with ISARA DS"):
    if func is None:
        return partial(handle_error, msg=msg)

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except IsaraException as ex:
            Except.throw_exception("ISARACommandError", ex.error_message, "")
        except Exception as e:
            self.set_state(DevState.FAULT)
            self.set_status(f"{msg}, error was: {e}")
            raise RuntimeError(f"{func.__name__}: {msg} Exception: {e}")

    return wrapper


def _tools_numbers_desc(isara: IsaraBaseConnectionClient) -> str:
    #
    # generate a human-readable table of tool numbers
    #

    def gen_lines():
        for num, name in isara.TOOL_MAPPING.items():
            yield f"{num}: {name}"

    txt = "\n".join(gen_lines())
    return f"tools numbers\n{txt}"


class ISARA(Device):
    """Tango device server for Isara robot."""

    # -----         device properties         ---- #
    host = device_property(dtype=str, mandatory=True)
    operate_port = device_property(dtype=int, default_value=10000)
    monitor_port = device_property(dtype=int, default_value=1000)

    # The length of time ISARA network command replies are kept in the internal cache.
    # Specifies number of seconds before cached value is re-fetched.
    command_cache_timeout = device_property(dtype=float, default_value=1.0)

    timeout = device_property(dtype=float, default_value=3)
    xpos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference X position in soaking position",
    )
    ypos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference Y position in soaking position",
    )
    zpos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference Z position in soaking position",
    )
    rxpos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference RX position in soaking position",
    )
    rypos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference RY position in soaking position",
    )
    rzpos_soak = device_property(
        dtype=float,
        default_value=0,
        doc="Reference RZ position in soaking position",
    )

    # -----         device methods            ---- #

    def __init__(self, *args, **kwargs) -> None:
        self.current_tool = None
        self._fault_dict: dict[str, bool | None] = {}
        self.isara: IsaraBaseConnectionClient | None = None
        super().__init__(*args, **kwargs)

    def init_device(self):
        """Initialize Tango device server."""
        super().init_device()
        self.set_state(DevState.INIT)
        self.current_tool = None
        self._fault_dict = {
            "in_fault": None,
            "last_msg": None,
            "doors_opened": None,
        }

        try:
            self.isara = connect(
                self.host,
                self.operate_port,
                self.monitor_port,
                self.command_cache_timeout,
            )
            self.dev_status()
            self._init_attributes()
            self._init_commands()
            self.current_tool = self.isara.get_attribute_value("Tool")
        except CommunicationError as e:
            # TODO: include error in 'status' device attribute
            print(e)
            self.set_state(DevState.FAULT)

    def delete_device(self):
        self.isara.disconnect()

    def _init_commands(self):
        #
        # add model specific commands
        #

        # add 'ChangeTool' command, with model dependant 'doc_in' string
        self.add_command(
            command(
                self.ChangeTool, dtype_in=int, doc_in=_tools_numbers_desc(self.isara)
            )
        )

    def _init_attributes(self):
        for attr_name, attr_type in self.isara.get_available_attributes():
            attr = attribute(
                name=attr_name,
                dtype=attr_type,
                access=AttrWriteType.READ,
            )
            self.add_attribute(attr, self._read_attr)

        #
        # create the special case 'PuckPresence' attribute,
        # this attribute's value is read via a method call to
        # the client library
        #
        attr = attribute(
            name="CassettePresence",
            dformat=AttrDataFormat.SPECTRUM,
            max_dim_x=IsaraBaseConnectionClient.PUCKS_NUM,
            dtype=CmdArgType.DevBoolean,
            access=AttrWriteType.READ,
        )
        self.add_attribute(attr, self._read_puck_presence_attr)

    @handle_error
    def _read_attr(self, attr):
        val = self.isara.get_attribute_value(attr.get_name())
        attr.set_value(val)

    @handle_error
    def _read_puck_presence_attr(self, attr):
        attr.set_value(self.isara.get_puck_presence())

    def _update_fault_dict(self, message, fault_status) -> None:
        # Check if default status bit is high (fault)
        # but ignore fault for certain status messages.
        # Store fault state, last error message and doors opened status
        # in fault_dict.
        fault_msgs_to_ignore = (
            "doors opened",
            "gripper drying in progress",
            "manual brake control selected",
            "disable when path is running",
            "ln2: external dewar overfill detected (from ami controller or pt100)",
            "low level alarm",
            "no ln2 available, regulation stopped",
        )  # OBS! Use lower case
        ignore_fault_msg = message in fault_msgs_to_ignore or len(message) == 0
        doors_opened_in_msg = "doors opened" in message
        if not fault_status:
            self._fault_dict["in_fault"] = False
            self._fault_dict["last_msg"] = None
            self._fault_dict["doors_opened"] = False
        else:
            if doors_opened_in_msg or self._fault_dict["doors_opened"]:
                self._fault_dict["doors_opened"] = True
            if not ignore_fault_msg:
                self._fault_dict["last_msg"] = message
            if self._fault_dict["last_msg"] is not None:
                self._fault_dict["in_fault"] = True

    def dev_status(self):
        names = (
            "Powered",
            "Tool",
            "PositionName",
            "Path",
            "PathRunning",
            "PathSafe",
            "PuckNumberOnTool",
            "SampleNumberOnTool",
            "PuckNumberOnToolB",
            "SampleNumberOnToolB",
            "Barcode",
            "PuckNumberOnDiff",
            "SampleNumberOnDiff",
            "PlateNumberOnTool",
            "LN2Regulation",
            "RemoteMode",
            "FaultStatus",
            "SpeedRatio",
            "CurrentNumberOfSoaking",
            "Message",
        )
        tokens = []
        for name in names:
            try:
                value = self.isara.get_attribute_value(name)
            except NoAttributeWithName:
                pass
            else:
                tokens.append(f"{name}({value})")
        new_status = "\n".join(tokens)

        self._update_fault_dict(
            self.isara.get_attribute_value("Message").strip().lower(),
            self.isara.get_attribute_value("FaultStatus"),
        )

        new_state = DevState.ON
        if self._fault_dict["in_fault"]:
            new_state = DevState.FAULT
            new_status = (
                "ISARA system reported a fault state.\n"
                + self._fault_dict["last_msg"]
                + "\n"
                + new_status
            )
        elif self.isara.get_attribute_value("Path") != "":
            new_state = DevState.RUNNING
        elif not self.isara.get_attribute_value("Powered"):
            new_state = DevState.OFF
            new_status = "ISARA system is OFF.\n" + new_status
        self.set_state(new_state)
        self.set_status(new_status)

        return self.get_status()

    # -----        attributes methods         ---- #

    def is_in_soak(self):
        if self.xpos_soak:
            x_pos = self.isara.get_attribute_value("X")
            y_pos = self.isara.get_attribute_value("Y")
            z_pos = self.isara.get_attribute_value("Z")
            rx_pos = self.isara.get_attribute_value("RX")
            ry_pos = self.isara.get_attribute_value("RY")
            rz_pos = self.isara.get_attribute_value("RZ")
            x_in_soak = self.xpos_soak - 1 < x_pos < self.xpos_soak + 1
            y_in_soak = self.ypos_soak - 1 < y_pos < self.ypos_soak + 1
            y_in_soak = self.zpos_soak - 1 < z_pos < self.zpos_soak + 1
            rx_in_soak = self.rxpos_soak - 1 < rx_pos < self.rxpos_soak + 1
            ry_in_soak = self.rypos_soak - 1 < ry_pos < self.rypos_soak + 1
            rz_in_soak = self.rzpos_soak - 1 < rz_pos < self.rzpos_soak + 1

            value = (
                x_in_soak & y_in_soak & y_in_soak & rx_in_soak & ry_in_soak & rz_in_soak
            )
        else:
            value = False
        return value

    # -----         device commands           ---- #

    @handle_error
    @command(dtype_out=str)
    def PowerOn(self):
        """Switch the robot power on"""
        return self.isara.power_on()

    @handle_error
    @command(dtype_out=str)
    def PowerOff(self):
        """Switch the robot power off"""
        return self.isara.power_off()

    @handle_error
    @command(dtype_out=str)
    def SpeedUp(self):
        """Increase robot speed ratio"""
        return self.isara.speed_up()

    @handle_error
    @command(dtype_out=str)
    def SpeedDown(self):
        """Decrease robot speed ratio"""
        return self.isara.speed_down()

    @handle_error
    @command(dtype_out=str)
    def Panic(self):
        """Stop the robot immediately, abort the trajectory and shut arm power of"""
        return self.isara.panic()

    @handle_error
    @command(dtype_out=str)
    def Abort(self):
        """Abort the trajectory"""
        return self.isara.abort()

    @handle_error
    @command(dtype_out=str)
    def Pause(self):
        """Stop the robot in position"""
        return self.isara.pause()

    @handle_error
    @command(dtype_out=str)
    def Restart(self):
        """Resume the trajectory after pause or default"""
        return self.isara.restart()

    @handle_error
    @command(dtype_out=str)
    def Reset(self):
        """Fault reset: acknowledge security fault and allow user to bring power back"""
        return self.isara.reset()

    @handle_error
    @command(dtype_out=str)
    def ClearBarcode(self):
        """clear the barcode/datamatrix return message"""
        return self.isara.clearbcrd()

    @handle_error
    @command(dtype_out=str)
    def OpenLid(self):
        """open lid"""
        return self.isara.openlid()

    @handle_error
    @command(dtype_out=str)
    def CloseLid(self):
        """Close lid"""
        return self.isara.closelid()

    @handle_error
    @command(dtype_out=str)
    def OpenTool(self):
        """Open the current gripper (jaw A in case of double gripper)"""
        return self.isara.opentool()

    @handle_error
    @command(dtype_out=str)
    def CloseTool(self):
        """Close the current gripper (jaw A in case of double gripper)"""
        return self.isara.closetool()

    @handle_error
    @command(dtype_out=str)
    def OpenToolB(self):
        """Open the current gripper (jaw B in case of double gripper)"""
        return self.isara.opentoolb()

    @handle_error
    @command(dtype_out=str)
    def CloseToolB(self):
        """Close the current gripper (jaw B in case of double gripper)"""
        return self.isara.closetoolb()

    @handle_error
    @command(dtype_out=str)
    def MagnetOn(self):
        """Switch on the magnetization of the goniometer electro-magnet"""
        return self.isara.magneton()

    @handle_error
    @command(dtype_out=str)
    def MagnetOff(self):
        """Switch off the magnetization of the goniometer electro-magnet"""
        return self.isara.magnetoff()

    @handle_error
    @command(dtype_out=str)
    def HeaterOn(self):
        """Switch on the heater"""
        return self.isara.heateron()

    @handle_error
    @command(dtype_out=str)
    def HeaterOff(self):
        """Switch off the heater"""
        return self.isara.heateroff()

    @handle_error
    @command(dtype_out=str)
    def Regulon(self):
        """Start LN2 level regulation in Dewar"""
        return self.isara.regulon()

    @handle_error
    @command(dtype_out=str)
    def RegulOff(self):
        """Stop LN2 level regulation in Dewar"""
        return self.isara.reguloff()

    # Maintenance commands

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def SetOnDiff(self, argin):
        """Assume that the specified sample is mounted on diffractometer"""
        puck, sample = argin
        return self.isara.setdiffr(puck, sample)

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def SetOnTool(self, argin):
        """Assume that the specified sample is currently in the gripper jaw"""
        puck, sample = argin
        return self.isara.settool(puck, sample)

    @handle_error
    @command(dtype_out=str)
    def ClearMemory(self):
        """Assume there are no sample nor plate in the tool or on the diffractometer"""
        return self.isara.clearmemory()

    # Trajectories commands

    @handle_error
    def ChangeTool(self, tool_number, dtype_out=str):
        """
        Launch automatic tool change, the robot will put its current tool on
        the parking and pick the one given in argument
        """
        return self.isara.change_tool(tool_number)

    @handle_error
    @command(dtype_out=str)
    def Home(self):
        """Move the robot arm back to home position"""
        return self.isara.home()

    @handle_error
    @command(dtype_out=str)
    def Recover(self):
        """
        Move the robot arm back to home position using a specific recover path which
        depends on the current area of the robot (diffractometer, dewar, drying...)
        """
        return self.isara.recover()

    @handle_error
    @command(dtype_out=str)
    def Back(self):
        """
        Put the sample in the gripper back in the Dewar to its
        memorized position (generally used after a "recover" path)
        """
        return self.isara.back()

    @handle_error
    @command(dtype_out=str)
    def BackHot(self):
        """
        Put the sample in the gripper back in the hot put to its
        memorized position (generally used after a "recover" path)
        """
        return self.isara.backht()

    @handle_error
    @command(dtype_out=str)
    def Soak(self):
        """
        Chilling of the gripper (only for grippers with process requiring drying and soaking phases)
        """
        return self.isara.soak()

    @handle_error
    @command(dtype_out=str)
    def Dry(self):
        """Dry the gripper <! Do not dry a gripper already in warm conditions !>"""
        return self.isara.dry()

    @handle_error
    @command(dtype_out=str)
    def TeachGonio(self):
        """
        Launch automatic teaching of goniometer position (available only with laser tool)
        """
        return self.isara.teachgonio()

    @handle_error
    @command(dtype_in=str, dtype_out=str)
    def TeachPuck(self, argin):
        """
        Launch automatic teaching of the puck given in argument (available only with laser tool)
        """
        return self.isara.teachgonio(argin)

    @handle_error
    @command(dtype_in=str, dtype_out=str)
    def TeachDewar(self, argin):
        """
        Launch automatic teaching of all puck positions inside the dewar, starting from
        the puck number specified (available only with laser tool)
        """
        return self.isara.teachdewar(argin)

    # sample operations

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def Put(self, argin):
        """Puts a new sample on the goniometer"""
        puck, sample = argin
        return self.isara.put(puck, sample)

    @handle_error
    @command(dtype_out=str)
    def Get(self):
        """Retrives sample currently on goniometer"""
        return self.isara.get()

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def GetPut(self, argin):
        """Retrives sample currently on goniometer and puts a new sample"""
        puck, sample = argin
        return self.isara.getput(puck, sample)

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def Pick(self, argin):
        """
        Pick a sample for the next mounting, eventually read its datamatrix and go
        back to soaking position (only available for double grippers with process
        requiring soaking phases)
        """
        puck, sample = argin
        return self.isara.pick(puck, sample)

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def Datamatrix(self, argin):
        """
        Take a sample from the Dewar, read the datamatrix and put the
        sample back into the Dewar
        """
        puck, sample = argin
        return self.isara.datamatrix(puck, sample)

    # hot sample operations

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def PutHot(self, argin):
        """Puts a new sample on the goniometer from the hot puck"""
        puck, sample = argin
        return self.isara.putht(puck, sample)

    @handle_error
    @command(dtype_out=str)
    def GetHot(self):
        """Retrives sample currently on goniometer"""
        return self.isara.getht()

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def GetPutHot(self, argin):
        """Retrives sample currently on goniometer and puts a new sample"""
        puck, sample = argin
        return self.isara.getputht(puck, sample)

    @handle_error
    @command(dtype_in=CmdArgType.DevVarStringArray, dtype_out=str)
    def GoToDiff(self, argin):
        """
        Take a sample from the Dewar and move to the goniometer mounting
        position without releasing it (path to test goniometer position)
        """
        puck, sample = argin
        return self.isara.gotodiff(puck, sample)

    @handle_error
    @command(dtype_out=str)
    def ToolCal(self, argin):
        """
        Take a sample from the Dewar and move to the goniometer mounting
        position without releasing it (path to test goniometer position)
        """
        return self.isara.toolcal()


def main():
    ISARA.run_server()


if __name__ == "__main__":
    main()
