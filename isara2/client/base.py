"""Base connection client for ISARA robots."""

import builtins
import re
import typing
from abc import (
    ABC,
    abstractmethod,
)
from collections.abc import Iterator
from dataclasses import dataclass
from threading import Lock
from typing import (
    Generic,
    TypeVar,
)

from typing_extensions import TypeIs

from .commands import (
    CommandCache,
    CommandSocket,
)
from .errors import (
    NoAttributeWithName,
    NoToolException,
)
from .parser import (
    NoMatchError,
    parse_bool,
    parse_reply,
)

T = TypeVar("T", bool, float, int, str)


@dataclass
class _Attr(Generic[T]):
    """Status attribute.

    Represents a value from a status command (``di``, ``do``, ``state``, and so on).

    Attributes:
        command: API command used to retrieve the attribute's value.
        position: Position of the attribute's value in the command's reply.
        data_type: Data type of the attribute, and associated Tango attribute, if any.
        name: Name of the associated Tango attribute.
            Optional, if not set or set to `None`, then no Tango attribute is created.
    """

    command: str
    position: int
    data_type: type[T]
    name: str | None = None

    def parse(self, token: str) -> T:
        """Parse value out of command's reply token."""

        def is_bool(raw_type) -> TypeIs[type[bool]]:
            return raw_type is builtins.bool

        def is_float(raw_type) -> TypeIs[type[float]]:
            return raw_type is builtins.float

        def is_int(raw_type) -> TypeIs[type[int]]:
            return raw_type is builtins.int

        def is_str(raw_type) -> TypeIs[type[str]]:
            return raw_type is builtins.str

        if is_bool(self.data_type):
            return parse_bool(token)
        if is_int(self.data_type):
            return int(token)
        if is_float(self.data_type):
            return float(token)
        if is_str(self.data_type):
            return str(token)
        raise TypeError(f"Can not parse to type '{self.data_type}' (token '{token}')")


class IsaraBaseConnectionClient(ABC):
    """Base connection client for ISARA robots.

    Attributes:
        model_name: A name indicating the model. Class attribute.
            Must be overridden in subclasses (for example "isara1" or "isara2").
        PUCKS_NUM: Number of pucks. Class attribute.
        TOOL_MAPPING: Map tool number (int) to tool name (str). Class attribute.
            Must be overridden in subclasses.
    """

    model_name: str

    PUCKS_NUM: int = 29

    TOOL_MAPPING: dict[int, str]

    #: List of status-related attributes.
    _ATTRIBUTES: list[_Attr] = [
        _Attr("state", 0, bool, "Powered"),
        _Attr("state", 1, bool, "RemoteMode"),
        _Attr("state", 2, bool, "FaultStatus"),
        _Attr("state", 3, str, "Tool"),
    ]

    #: Expected format of the reply to the ``di`` command.
    _DI_REPLY_RE = re.compile(r"^di\((.+)\)$")

    #: Expected format of the reply to the ``do`` command.
    _DO_REPLY_RE = re.compile(r"^do\((.+)\)$")

    #: Expected format of the reply to the ``state`` command.
    _STATE_REPLY_RE = re.compile(r"^state\((.+)\)$")

    def __init__(self, process_sock: CommandSocket, status_sock: CommandSocket):
        """Initialize instance."""
        self._attributes = IsaraBaseConnectionClient._ATTRIBUTES
        self._command_caches: dict[str, CommandCache] = {}

        self.process_sock = process_sock
        self.status_sock = status_sock
        self.op_lock = Lock()
        self.mon_lock = Lock()
        self.current_tool = None

        # info for checking path safe condition for diffractometer
        self.pathinfo = {"safe": False, "running": False}

    def _add_command_cache(self, command_name: str, command_cache: CommandCache):
        self._command_caches[command_name] = command_cache

    def _invalidate_status_command_caches(self):
        """Invalidate caches for status-related commands."""
        for command_cache in self._command_caches.values():
            command_cache.invalidate()

    def get_available_attributes(self) -> Iterator[tuple[str, type]]:
        """Iterate through map of Tango attribute to data type.

        Only attributes that are meant to be "exported" as Tango attributes are yielded.
        """
        for attr in self._ATTRIBUTES:
            if attr.name:
                # Export only if the attribute has a name.
                yield attr.name, attr.data_type

    def _get_attribute_by_name(self, name: str) -> _Attr:
        for attr in self._ATTRIBUTES:
            if attr.name == name:
                return attr
        raise NoAttributeWithName(f"No attribute with name: '{name}'.")

    def get_attribute_value(self, name: str):
        """Get value for attribute by its name."""
        attr = self._get_attribute_by_name(name)
        value = self._command_caches[attr.command].value[attr.position]
        return value

    def disconnect(self):
        """Disconnect."""
        # TODO: think about disconnect
        try:
            self.process_sock.close()
            self.status_sock.close()
        except Exception:
            pass
        self.process_sock = None
        self.status_sock = None

    # Status commands

    def _monitor(self, cmd: str) -> str:
        with self.mon_lock:
            return self.status_sock.send_command(cmd)

    def _parse_all(
        self,
        command: str,
        tokens: list[str],
    ) -> Iterator[typing.Any]:
        for index, token in enumerate(tokens):
            for attr in self._ATTRIBUTES:
                if command == attr.command and index == attr.position:
                    yield attr.parse(token)
                    break
            else:
                yield None

    def _get_status_command(
        self,
        command: str,
        pattern: re.Pattern,
        comma_separated: bool = True,
    ) -> list:
        """Helper for status commands (``di``, ``di2``, ``do``, ``state``)."""
        reply = self._monitor(command)
        try:
            (blob,) = parse_reply(pattern, reply)
        except NoMatchError as exc:
            raise NotImplementedError(f"Can not handle reply: '{reply}'.") from exc
        tokens = blob.split(",") if comma_separated else list(blob)
        return list(self._parse_all(command, tokens))

    def _state(self) -> list:
        return self._get_status_command("state", self._STATE_REPLY_RE)

    @abstractmethod
    def get_puck_presence(self) -> list[bool]:
        """Get presence of pucks as a list of booleans."""
        raise NotImplementedError

    # General commands

    def _operate(self, cmd):
        with self.op_lock:
            result = self.process_sock.send_command(cmd)
            self._invalidate_status_command_caches()
            return result

    def power_on(self):
        """Switch the robot power on."""
        return self._operate("on")

    def power_off(self):
        """Switch the robot ower off."""
        return self._operate("off")

    def speed_up(self):
        """Increase robot speed."""
        return self._operate("speedup")

    def speed_down(self):
        """Decrease robot speed."""
        return self._operate("speeddown")

    def panic(self):
        """Stop the robot immediately, abort the trajectory, and shut arm power off."""
        return self._operate("panic")

    def abort(self):
        """Abort the trajectory."""
        return self._operate("abort")

    def pause(self):
        """Stop the robot in position."""
        return self._operate("pause")

    def restart(self):
        """Resume the trajectory after pause or default."""
        return self._operate("restart")

    def reset(self):
        """Fault reset.

        Acknowledge security fault and allow user to bring power back.
        """
        return self._operate("reset")

    def clearbcrd(self):
        """Clear the barcode/datamatrix return message."""
        return self._operate("clearbcrd")

    @abstractmethod
    def openlid(self):
        """Open lid."""
        raise NotImplementedError

    @abstractmethod
    def closelid(self):
        """Close lid."""
        raise NotImplementedError

    def opentool(self):
        """Open the current gripper (jaw 1/A in case of double gripper)."""
        return self._operate("opentool")

    def closetool(self):
        """Close the current gripper (jaw 1/A in case of double gripper)."""
        return self._operate("closetool")

    @abstractmethod
    def opentoolb(self):
        """Open the current gripper (jaw 2/B in case of double gripper)."""
        raise NotImplementedError

    @abstractmethod
    def closetoolb(self):
        """Close the current gripper (jaw 2/B in case of double gripper)."""
        raise NotImplementedError

    def magneton(self):
        """Switch on the magnetization of the goniometer electro-magnet."""
        return self._operate("magneton")

    def magnetoff(self):
        """Switch off the magnetization of the goniometer electro-magnet."""
        return self._operate("magnetoff")

    def heateron(self):
        """Switch on the heater."""
        return self._operate("heateron")

    def heateroff(self):
        """Switch off the heater."""
        return self._operate("heateroff")

    # Trajectory commands

    def _get_current_tool(self) -> int:
        state = self._state()  # avoiding cmd cache to get the very latest info
        tool_name = state[3]
        for number, name in self.TOOL_MAPPING.items():
            if tool_name == name:
                return number
        raise NoToolException

    def _traj(self, args):
        with self.op_lock:
            result = self.process_sock.send_command(args)
            self._invalidate_status_command_caches()
            return result

    @abstractmethod
    def home(self, tool_number: int | None = None) -> str:
        """Move the robot arm back to home position."""
        raise NotImplementedError

    @abstractmethod
    def recover(self, tool_number: int | None = None) -> str:
        """Move the robot arm back to home position with dedicated recovery path.

        Move the robot arm back to home position using a specific recover path
        which depends on the current area of the robot
        (diffractometer, dewar, drying...).
        """
        raise NotImplementedError

    @abstractmethod
    def put(self, puck, sample, datamatrix_scan: int = 0) -> str:
        """Take a sample from the Dewar.

        Eventually read its datamatrix and mount it on the goniometer.
        """
        raise NotImplementedError

    @abstractmethod
    def get(self, datamatrix_scan=0):
        """Get the sample from the diffractometer.

        Eventually read its datamatrix and put it back into the Dewar,
        in its memorized position.
        """
        raise NotImplementedError

    @abstractmethod
    def getput(self, puck, sample, datamatrix_scan=0):
        """Swap samples.

        Get the sample currently mounted on the goniometer, put it back into the Dewar,
        and mount the specified sample on the goniometer
        eventually reading its datamatrix.
        No heating of the gripper between both operations.
        """
        raise NotImplementedError

    @abstractmethod
    def pick(self, puck, sample, datamatrix_scan=0):
        """Pick a sample for the next mounting.

        Pick a sample for the next mounting,
        eventually read its datamatrix and go back to soaking position
        (only available for double grippers requiring soaking phases).
        """
        raise NotImplementedError

    @abstractmethod
    def datamatrix(self, puck, sample):
        """Read sample datamatrix.

        Take a sample from the Dewar, read the datamatrix,
        and put the sample back into the Dewar.
        """
        raise NotImplementedError

    @abstractmethod
    def back(self, tool_number: int | None = None) -> str:
        """Move sample in gripper back to Dewar.

        Put the sample in the gripper back in the Dewar
        to its memorized position (generally used after a "recover" path).
        """
        raise NotImplementedError

    @abstractmethod
    def soak(self, tool_number: int | None = None) -> str:
        """Chill the gripper.

        Go to soak position.
        Only for grippers with process requiring drying and soaking phases.
        """
        raise NotImplementedError

    @abstractmethod
    def dry(self, tool_number: int | None = None) -> str:
        """Dry the gripper."""
        raise NotImplementedError

    @abstractmethod
    def gotodiff(self, puck, sample):
        """Bring sample to goniometer mounting position.

        Take a sample from the Dewar and move to the goniometer mounting
        position without releasing it (path to test goniometer position)
        """
        raise NotImplementedError

    @abstractmethod
    def teachgonio(self, tool_number: int | None = None) -> str:
        """Launch automatic teaching of goniometer position.

        This is available with the laser tool only.
        """
        raise NotImplementedError

    @abstractmethod
    def teachpuck(self, puck, tool_number: int | None = None) -> str:
        """Launch automatic teaching of the puck given in argument.

        This is available with the laser tool only.
        """
        raise NotImplementedError

    @abstractmethod
    def teachdewar(self, puck, tool_number: int | None = None) -> str:
        """Launch automatic teaching of all puck positions inside the dewar.

        This is available with the laser tool only.

        Args:
            puck: starting from the puck number specified
            tool_number: tool number
        """
        raise NotImplementedError

    @abstractmethod
    def change_tool(self, tool_number: int) -> str:
        """Launch automatic tool change.

        Launch automatic tool change,
        the robot will put its current tool on the parking
        and pick the tool given in argument.
        """
        raise NotImplementedError

    @abstractmethod
    def toolcal(self):
        """Calibrate tool.

        Pick up the calibration pin,
        and start gripper or laser tool calibration
        until the precision criterion is reached.
        """
        raise NotImplementedError

    # Hot puck commands

    @abstractmethod
    def putht(self, puck, sample, datamatrix_scan=0):
        """Pick a sample from designated hot puck.

        Pick a sample from designated hot puck,
        eventually reads its datamatrix and mount it on the goniometer.
        """
        raise NotImplementedError

    @abstractmethod
    def getht(self, datamatrix_scan=0):
        """Get sample back into hot puck.

        Get the sample from the diffractometer, eventually read its datamatrix,
        and put it back into the hot puck in its memorized position.
        """
        raise NotImplementedError

    @abstractmethod
    def getputht(self, puck, sample, datamatrix_scan=0):
        """Get mounted sample back to hot puck and mount new sample.

        Get the sample currently mounted on the goniometer,
        put it back into the hot puck,
        and mount the specified sample on the goniometer
        eventually reading its datamatrix
        (no heating of the gripper between both operations).
        """
        raise NotImplementedError

    @abstractmethod
    def backht(self, tool_number=None):
        """Move sample in gripper back to hot puck.

        Put the sample in the gripper back onto the hot puck
        in its memorized position (generally used after a "recover" path).
        """
        raise NotImplementedError

    @abstractmethod
    def teachhotpuck(self, puck, tool_number=None):
        """Launch automatic teaching of the hot puck given in argument."""
        raise NotImplementedError

    # LN2 controller commands

    def regulon(self):
        """Start LN2 level regulation in Dewar."""
        return self._operate("regulon")

    def reguloff(self):
        """Stop LN2 level regulation in Dewar."""
        return self._operate("reguloff")

    # Maintenance commands

    def setdiffr(self, puck, sample):
        """Assume that the specified sample is mounted on diffractometer."""
        cmd = f"setdiffr({puck},{sample},0)"
        return self._operate(cmd)

    def settool(self, puck, sample):
        """Assume that the specified sample is currently in the gripper jaw."""
        cmd = f"settool({puck},{sample},0,0)"
        return self._operate(cmd)

    def clearmemory(self):
        """Assume there are no sample nor plate in the tool or on the diffractometer."""
        return self._operate("clearmemory")

    # Checks for unexpected commands

    def is_sample_on_diff(self):
        if self.get_attribute_value("SampleNrOnDiff") == "":
            raise Exception(
                "Put operation not authorized while a sample is detected on magnet"
            )

    def _is_in_soak(self):
        return self.get_attribute_value("Path") == "Soak"
