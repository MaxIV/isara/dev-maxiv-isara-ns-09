"""Parsing helpers."""

from re import Pattern


class NoMatchError(Exception):
    """Reply does not match expected pattern."""


class UnexpectedBoolStringError(Exception):
    """Unexpected boolean string value (not '0' or '1')."""


def parse_bool(text: str) -> bool:
    """Parse boolean value out of string."""
    if text == "0":
        return False
    if text == "1":
        return True

    raise UnexpectedBoolStringError(f"Expected '0' or '1', got '{text}'")


def parse_reply(regexp: Pattern, reply: str) -> tuple[str, ...]:
    """Parse command reply according to regular expression pattern."""
    match = regexp.match(reply)
    if match is None:
        raise NoMatchError()

    return match.groups()
