"""Connection client for robot model "ISARA2"."""

from typing_extensions import override

from .base import (
    IsaraBaseConnectionClient,
    _Attr,
)
from .commands import (
    CommandCache,
    CommandSocket,
)


class Isara2ConnectionClient(IsaraBaseConnectionClient):
    """Connection client for robot model "ISARA2"."""

    # Override
    model_name: str = "isara2"

    # Override
    TOOL_MAPPING = {
        0: "ToolChanger",
        1: "Cryotong",
        2: "SingleGripper",
        3: "DoubleGripper",
        4: "MiniSpineGripper",
        5: "RotatingGripper",
        6: "PlateGripper",
        7: "Spare",
        8: "LaserTool",
    }

    # Override
    _ATTRIBUTES = [
        *IsaraBaseConnectionClient._ATTRIBUTES,
        # state
        _Attr("state", 4, str, "PositionName"),
        _Attr("state", 5, str, "Path"),
        _Attr("state", 8, str, "PuckNumberOnTool"),
        _Attr("state", 9, str, "SampleNumberOnTool"),
        _Attr("state", 10, str, "PuckNumberOnToolB"),
        _Attr("state", 11, str, "SampleNumberOnToolB"),
        _Attr("state", 12, int, "PuckNumberOnDiff"),
        _Attr("state", 13, int, "SampleNumberOnDiff"),
        _Attr("state", 14, str, "PlateNumberOnTool"),
        _Attr("state", 15, str, "PlateNumberOnDiff"),
        _Attr("state", 16, str, "Barcode"),
        _Attr("state", 17, bool, "PathRunning"),
        _Attr("state", 18, bool, "PathPause"),
        _Attr("state", 19, float, "SpeedRatio"),
        _Attr("state", 20, bool, "LN2Regulation"),
        _Attr("state", 21, str, "CurrentNumberOfSoaking"),
        _Attr("state", 22, float, "LN2Level"),
        _Attr("state", 26, bool, "GripperDrying"),
        _Attr("state", 28, str, "Message"),
        _Attr("state", 29, str, "BinaryAlarms"),
        _Attr("state", 30, float, "X"),
        _Attr("state", 31, float, "Y"),
        _Attr("state", 32, float, "Z"),
        _Attr("state", 33, float, "RX"),
        _Attr("state", 34, float, "RY"),
        _Attr("state", 35, float, "RZ"),
        _Attr("state", 44, str, "CryoVisionData"),
        _Attr("state", 46, bool, "HeatingCablesOn"),
        # do
        _Attr("do", 24, bool, "SampleDetectedOnGonio"),
        _Attr("do", 43, bool, "PathSafe"),  # Arm is parked
    ]

    #: Offset to the first ``doPuckPresence`` element in ``do`` reply.
    _DO_PUCK_PRESENCE_OFFSET = 56

    def __init__(
        self,
        process_sock: CommandSocket,
        status_sock: CommandSocket,
        command_cache_timeout: float,
    ):
        """Initialize instance."""
        super().__init__(process_sock, status_sock)

        self._add_command_cache(
            "state", CommandCache(self._state, command_cache_timeout)
        )
        self._add_command_cache("do", CommandCache(self._do, command_cache_timeout))
        self._add_command_cache("di", CommandCache(self._di, command_cache_timeout))

    # Status commands

    def _di(self) -> list[bool]:
        return self._get_status_command("di", self._DI_REPLY_RE)

    def _do(self) -> list[bool]:
        return self._get_status_command("do", self._DO_REPLY_RE)

    @override
    def get_puck_presence(self):
        do_vals = self._command_caches["do"].value
        #
        # return the slice of 'do' reply that contains
        # 'doPuckPresence' elements
        #
        first = self._DO_PUCK_PRESENCE_OFFSET
        last = first + self.PUCKS_NUM
        return do_vals[first:last]

    # General commands

    @override
    def openlid(self):
        return self._operate("openlid")

    @override
    def closelid(self):
        return self._operate("closelid")

    @override
    def opentoolb(self):
        return self._operate("opentoolb")

    @override
    def closetoolb(self):
        return self._operate("closetoolb")

    # Trajectory commands

    @override
    def home(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()

        cmd = f"traj(home,{tool_number})"
        return self._traj(cmd)

    @override
    def recover(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()

        cmd = f"traj(recover,{tool_number})"
        return self._traj(cmd)

    @override
    def put(self, puck, sample, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        args = f"{tool_number},{puck},{sample},{datamatrix_scan},0,0,0,0,0,0,0,0,0"
        cmd = f"traj(put,{args})"
        return self._traj(cmd)

    @override
    def get(self, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        cmd = f"traj(get,{tool_number},0,0,{datamatrix_scan},0,0,0,0,0,0,0,0,0)"
        return self._traj(cmd)

    @override
    def getput(self, puck, sample, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        args = f"{tool_number},{puck},{sample},{datamatrix_scan},0,0,0,0,0,0,0,0,0"
        cmd = f"traj(getput,{args})"
        return self._traj(cmd)

    @override
    def pick(self, puck, sample, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        cmd = f"traj(pick,{tool_number},{puck},{sample},{datamatrix_scan},0,0,0,0,0)"
        return self._traj(cmd)

    @override
    def datamatrix(self, puck, sample):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        cmd = f"traj(datamatrix,{tool_number},{puck},{sample},0,0,0,0,0,0,0)"
        return self._traj(cmd)

    @override
    def back(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()

        cmd = f"traj(back,{tool_number})"
        return self._traj(cmd)

    @override
    def soak(self, tool_number=None):
        if self._is_in_soak():
            raise Exception("Soak operation not executed, already in Soak")
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(soak,{tool_number})"
        return self._traj(cmd)

    @override
    def dry(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(dry,{tool_number})"
        return self._traj(cmd)

    @override
    def teachgonio(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(teachgonio,{tool_number})"
        return self._traj(cmd)

    @override
    def teachpuck(self, puck, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(teachpuck,{tool_number},{puck})"
        return self._traj(cmd)

    @override
    def teachdewar(self, puck, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(teachdewar,{tool_number},{puck})"
        return self._traj(cmd)

    @override
    def change_tool(self, tool_number):
        cmd = f"traj(changetool,{tool_number})"
        return self._traj(cmd)

    @override
    def toolcal(self):
        tool_number = self._get_current_tool()

        cmd = f"traj(toolcal,{tool_number})"
        return self._traj(cmd)

    @override
    def gotodiff(self, puck, sample):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        cmd = f"traj(gotodiff,{tool_number},{puck},{sample},0,0,0,0,0,0)"
        return self._traj(cmd)

    # Hot puck commands

    @override
    def putht(self, puck, sample, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        args = f"{tool_number},{puck},{sample},{datamatrix_scan},0,0,0,0,0,0,0,0,0"
        cmd = f"traj(putht,{args})"
        return self._traj(cmd)

    @override
    def getht(self, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        cmd = f"traj(getht,{tool_number},0,0,{datamatrix_scan},0,0,0,0,0,0,0,0,0)"
        return self._traj(cmd)

    @override
    def getputht(self, puck, sample, datamatrix_scan=0):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()

        args = f"{tool_number},{puck},{sample},{datamatrix_scan},0,0,0,0,0,0,0,0,0"
        cmd = f"traj(getputht,{args})"
        return self._traj(cmd)

    @override
    def backht(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()

        cmd = f"traj(backht,{tool_number})"
        return self._traj(cmd)

    @override
    def teachhotpuck(self, puck, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"traj(teachhotpuck,{tool_number},{puck})"
        return self._traj(cmd)
