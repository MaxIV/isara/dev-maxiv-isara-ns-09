from isara2.client.commands import CommandSocket

from .base import IsaraBaseConnectionClient
from .isara1 import Isara1ConnectionClient
from .isara2 import Isara2ConnectionClient


class CommunicationError(Exception):
    """Handle communication error"""

    def __init__(self, error=None):
        self.error = error

    def __str__(self):
        return f"CommunicationError: {self.error}."


def _probe_model(status_sock: CommandSocket):
    """
    Probe ISARA Sample Changer model via status socket.

    Returns a client class that should be used for connected to the
    detected ISARA model.
    """

    #
    # probe model by count number of 'state' reply
    #
    reply = status_sock.send_command("state")

    num_els = len(reply.split(","))
    if num_els == 60:  # 60 element in the reply, must be ISARA2
        return Isara2ConnectionClient

    if num_els == 23:  # 23 element in the reply, must be ISARA1
        return Isara1ConnectionClient

    assert False, f"unexpected 'state' reply: '{reply}'"


def connect(
    host: str,
    process_port: int,
    status_port: int,
    command_cache_timeout: float,
) -> IsaraBaseConnectionClient:
    # connect to process and status sockets
    try:
        process_sock = CommandSocket(host, process_port)
        status_sock = CommandSocket(host, status_port)
    except OSError as e:
        raise CommunicationError(f"{e}")

    client_class = _probe_model(status_sock)
    print(f"using {client_class} to connect")

    return client_class(process_sock, status_sock, command_cache_timeout)
