import time
from telnetlib import Telnet

from isara2.client.errors import CommandErrorException


class CommandSocket:
    def __init__(self, host: str, port):
        self.sock = Telnet(host, port)

    def _read_reply(self):
        reply = self.sock.read_until(b"\r")
        reply = reply.decode().strip()
        print(f"received message: {reply}")

        return reply

    def send_command(self, command: str) -> str:
        """
        Sends command on this socket and reads the reply.

        Raises CommandErrorException exception on error reply from ISARA.
        """
        data = f"{command}\r".encode()
        print(f"sent message> {command}")
        self.sock.write(data)

        # command -> reply can be:
        # "setdiffr(1,1,0)" -> setdiffr
        # traj(put,3,1,5) -> put
        # home -> home
        # we need to extract the command name from the sent message
        if command.startswith("traj"):
            command = command.split("traj(")[1].split(",")[0]
        elif "(" in command:
            command = command.split("(")[0]

        print(f"actual command > {command}")

        reply = self._read_reply()
        if not reply.startswith(command) and command != "message":
            # ISARA replied with an error message, probably
            raise CommandErrorException(command, reply)

        return reply


class CommandCache:
    """Command cache."""

    def __init__(self, command, stale_timeout: float):
        """Initialize instance.

        Parameters:
            stale_timeout: Number of seconds before cached value is considered stale.
        """
        self._command = command
        self._value = None
        self._stale_timeout = stale_timeout
        self._timeout = None  # when value becomes stale

    def _value_is_valid(self):
        if self._value is None:
            return False

        now = time.monotonic()
        if now > self._timeout:
            # value have become stale
            return False

        return True

    def invalidate(self):
        """Invalidate cache."""
        # Set the timeout to a value in the past
        self._timeout = time.monotonic() - self._stale_timeout

    @property
    def value(self):
        if not self._value_is_valid():
            self._value = self._command()
            self._timeout = time.monotonic() + self._stale_timeout

        return self._value
