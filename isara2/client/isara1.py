"""Connection client for robot model "ISARA1" (BioMAX)."""

import dataclasses
import re

from typing_extensions import override

from .base import (
    IsaraBaseConnectionClient,
    _Attr,
)
from .commands import (
    CommandCache,
    CommandSocket,
)


@dataclasses.dataclass
class _IntAttrWithFallback(_Attr[int]):
    """Integer attribute with fallback value of `-1` for empty string."""

    # Override
    data_type: type[int] = int

    @override
    def parse(self, token: str) -> int:
        return -1 if "" == token else int(token)


class Isara1ConnectionClient(IsaraBaseConnectionClient):
    """Connection client for ISARA robot model "ISARA1" (BioMAX)."""

    # Override
    model_name: str = "isara1"

    # Override
    TOOL_MAPPING = {
        0: "Flange",
        1: "UnipuckGripper",
        2: "RotatingGripper",
        3: "PlateGripper",
        4: "LaserTool",
        5: "Double",
    }

    # Override
    _ATTRIBUTES = [
        *IsaraBaseConnectionClient._ATTRIBUTES,
        # di
        _Attr("di", 10, bool, "SampleDetectedOnGonio"),
        # di2
        *[
            _Attr("di2", i, bool) for i in range(0, IsaraBaseConnectionClient.PUCKS_NUM)
        ],  # Puck detection
        # do
        _Attr("do", 7, bool, "PathSafe"),  # Arm is parked
        # message
        _Attr("message", 0, str, "Message"),
        # state
        _Attr("state", 4, str, "Path"),
        _Attr("state", 5, str, "PuckNumberOnTool"),
        _Attr("state", 6, str, "SampleNumberOnTool"),
        _IntAttrWithFallback("state", 7, name="PuckNumberOnDiff"),
        _IntAttrWithFallback("state", 8, name="SampleNumberOnDiff"),
        _Attr("state", 9, str, "PlateNumberOnTool"),
        _Attr("state", 11, str, "Barcode"),
        _Attr("state", 12, bool, "PathRunning"),
        _Attr("state", 13, bool, "LN2Regulation"),
        _Attr("state", 15, float, "SpeedRatio"),
        _Attr("state", 16, float, "GonioX"),
        _Attr("state", 17, float, "GonioY"),
        _Attr("state", 18, float, "GonioZ"),
        _Attr("state", 20, str, "PuckNumberOnToolB"),
        _Attr("state", 21, str, "SampleNumberOnToolB"),
        _Attr("state", 22, str, "CurrentNumberOfSoaking"),
    ]

    #: Expected format of the reply to the ``di2`` command.
    _DI2_REPLY_RE = re.compile(r"^di2\((.+)\)$")

    def __init__(
        self,
        process_sock: CommandSocket,
        status_sock: CommandSocket,
        command_cache_timeout: float,
    ):
        """Initialize instance."""
        super().__init__(process_sock, status_sock)

        self._add_command_cache("di", CommandCache(self._di, command_cache_timeout))
        self._add_command_cache("di2", CommandCache(self._di2, command_cache_timeout))
        self._add_command_cache("do", CommandCache(self._do, command_cache_timeout))
        self._add_command_cache(
            "message", CommandCache(self._message, command_cache_timeout)
        )
        self._add_command_cache(
            "state", CommandCache(self._state, command_cache_timeout)
        )

    # Status commands

    def _di(self) -> list[bool]:
        return self._get_status_command("di", self._DI_REPLY_RE, comma_separated=False)

    def _di2(self) -> list[bool]:
        return self._get_status_command(
            "di2",
            self._DI2_REPLY_RE,
            comma_separated=False,
        )

    def _do(self) -> list[bool]:
        return self._get_status_command("do", self._DO_REPLY_RE, comma_separated=False)

    def _message(self) -> list[str]:
        return [self._monitor("message")]

    def _position(self):
        return self._monitor("position")

    @override
    def get_puck_presence(self):
        return self._command_caches["di2"].value

    # General commands

    @override
    def openlid(self):
        return self._operate("openlid1")

    @override
    def closelid(self):
        return self._operate("closelid1")

    @override
    def opentoolb(self):
        # Not available on Isara1 robot. But there is `closetool2`.
        raise NotImplementedError

    @override
    def closetoolb(self):
        # Not available on Isara1 robot. But there is `closetool2`.
        raise NotImplementedError

    # Trajectory commands

    @override
    def home(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"home({tool_number})"
        return self._traj(cmd)

    @override
    def recover(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"safe({tool_number})"
        return self._traj(cmd)

    @override
    def put(self, puck, sample, datamatrix_scan=0):
        tool_number = self._get_current_tool()
        if datamatrix_scan:
            cmd = f"put_bcrd({tool_number},{puck},{sample})"
        else:
            cmd = f"put({tool_number},{puck},{sample})"
        return self._traj(cmd)

    @override
    def get(self, datamatrix_scan=0):
        if datamatrix_scan != 0:
            raise NotImplementedError
        tool_number = self._get_current_tool()

        cmd = f"get({tool_number})"
        return self._traj(cmd)

    @override
    def getput(self, puck, sample, datamatrix_scan=0):
        if datamatrix_scan != 0:
            raise NotImplementedError
        tool_number = self._get_current_tool()
        cmd = f"getput({tool_number},{puck},{sample})"
        return self._traj(cmd)

    @override
    def pick(self, puck, sample, datamatrix_scan=0):
        if datamatrix_scan != 0:
            raise NotImplementedError

        tool_number = self._get_current_tool()

        if self.TOOL_MAPPING[tool_number] != "DoubleGripper":
            raise RuntimeError("Command only allowed for DoubleGripper")

        cmd = f"pick({tool_number},{puck},{sample})"
        return self._traj(cmd)

    @override
    def datamatrix(self, puck, sample):
        # ignoring next sample preloading
        tool_number = self._get_current_tool()
        cmd = f"barcode({tool_number},{puck},{sample})"
        return self._traj(cmd)

    @override
    def back(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"back({tool_number})"
        return self._traj(cmd)

    @override
    def soak(self, tool_number=None):
        # if self._is_in_soak():
        #     raise Exception("Soak operation not executed, already in Soak")
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"soak({tool_number})"
        return self._traj(cmd)

    @override
    def dry(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"dry({tool_number})"
        return self._traj(cmd)

    @override
    def gotodiff(self, puck, sample):
        tool_number = self._get_current_tool()
        cmd = f"gotodiff({tool_number},{puck},{sample})"
        return self._traj(cmd)

    @override
    def teachgonio(self, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"teach_gonio({tool_number})"
        return self._traj(cmd)

    @override
    def teachpuck(self, puck, tool_number=None):
        if tool_number is None:
            tool_number = self._get_current_tool()
        cmd = f"teach_puck({tool_number},{puck})"
        return self._traj(cmd)

    @override
    def teachdewar(self, puck, tool_number=None):
        raise NotImplementedError("No `teachdewar` command on Isara1")

    @override
    def change_tool(self, tool_number):
        cmd = f"home({tool_number})"
        return self._traj(cmd)

    @override
    def toolcal(self):
        tool_number = self._get_current_tool()
        cmd = f"toolcal({tool_number})"
        return self._traj(cmd)

    # Hot puck commands

    @override
    def putht(self, puck, sample, datamatrix_scan=0):
        raise NotImplementedError("No hot puck features on Isara1")

    @override
    def getht(self, datamatrix_scan=0):
        raise NotImplementedError("No hot puck features on Isara1")

    @override
    def getputht(self, puck, sample, datamatrix_scan=0):
        raise NotImplementedError("No hot puck features on Isara1")

    @override
    def backht(self, tool_number=None):
        raise NotImplementedError("No hot puck features on Isara1")

    @override
    def teachhotpuck(self, puck, tool_number=None):
        raise NotImplementedError("No hot puck features on Isara1")
