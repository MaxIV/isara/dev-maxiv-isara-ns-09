"""Exceptions."""


class IsaraException(Exception):
    """Base class for Isara exceptions.

    Derived classes must have a string attribute `error_message`.

    Attributes:
        error_message: Error message to be displayed to the user.
    """

    error_message: str


class CommandErrorException(IsaraException):
    """Raised on error reply from ISARA.

    Attributes:
        command: The command that was sent to Isara robot.
        error_message: The error reply from Isara robot.
    """

    def __init__(self, command: str, error_message: str):
        self.command = command
        self.error_message = error_message
        super().__init__(f"Command '{command}' failed: '{error_message}'.")


class NoToolException(IsaraException):
    """Raised when no tool mounted on Isara robot."""

    def __init__(self) -> None:
        self.error_message = "No tool on Isara robot"
        super().__init__(self.error_message)


class NoAttributeWithName(IsaraException):
    """Raised when trying to access an attribute whose name can not be found."""
