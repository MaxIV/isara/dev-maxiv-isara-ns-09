<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?package=tangods-isara2"/>

# ISARA sample changer

This repository implements a tango device server for controlling an IRELEC ISARA sample changer.

# ISARA Tango Device

This tango device server supports Isara1 and Isara2 models of sample changer systems.
The device server automatically detects the model on the start-up.
The aim of this implementation to provide same tango API for both Isara1 and Isara2 systems as much as possible.
However, the available tango attributes and commands are different, as not all features are
available on both Isara1 and Isara2 sample changes.

## Properties

The tango device is configured using properties below.

| property     | mandatory | default | purpose                                       |
|--------------|-----------|---------|-----------------------------------------------|
| host         | yes       | n/a     | network address of the ISARA device           |
| operate_port | no        | 10000   | TCP port number of the process control socket |
| monitor_port | no        | 1000    | TCP port number of the status feedback socket |

Note that the _process control_ socket is also referred as _Socket 1_ and
_status feedback_ socket as _Socket 2_ in IRELEC documentation.

## Attributes

Following attributes are available on both Isara1 and Isara2 systems:

| attribute              | description                                               |
|------------------------|-----------------------------------------------------------|
| Barcode                | last datamatrix read value                                |
| CurrentNumberOfSoaking | soaking phases number for grippers that need to be cooled |
| FaultStatus            | is system in fault or stop mode                           |
| LN2Regulation          | is LN2 regulation running                                 |
| Message                | last information message from PLC                         |
| Path                   | currently running path's name                             |
| PathRunning            | is path running                                           |
| PathSafe               | arm is parked                                             |
| PlateNumberOnTool      | number of the plate mounted on the tool                   |
| Powered                | is robot arm powered on                                   |
| PuckNumberOnDiff       | puck number of the sample mounted on diffractometer       |
| PuckNumberOnTool       | puck number of the sample mounted on jaw A tool           |
| PuckNumberOnToolB      | puck number of the sample mounted on jaw B tool           |
| RemoteMode             | is remote operating mode enabled                          |
| SampleDetectedOnGonio  | is a sample currently detected on the gonio               |
| SampleNumberOnDiff     | number of the sample mounted on diffractometer            |
| SampleNumberOnTool     | number of the sample on jaw A tool                        |
| SampleNumberOnToolB    | number of the sample on jaw B tool                        |
| SpeedRatio             | current robot arm speed ratio (%)                         |
| Tool                   | tool name                                                 |

Following attributes are available only on Isara2 systems:

| attribute             | description                                               |
|-----------------------|-----------------------------------------------------------|
| BinaryAlarms          | binary alarms flags                                       |
| CryoVisionData        | puck and sample presence info from cryo-vision controller |
| GripperDrying         | is gripper drying in progress                             |
| HeatingCablesOn       | are heating cables turned ON                              |
| LN2Level              | LN2 current level in the Dewar (%)                        |
| PathPause             | is path paused                                            |
| PlateNumberOnDiff     | number of the plate mounted on the diffractometer         |
| PositionName          | robot arm's current position name                         |
| RX                    | Rx coordinate of current cartesian position               |
| RY                    | Ry coordinate of current cartesian position               |
| RZ                    | Rz coordinate of current cartesian position               |
| X                     | X coordinate of current cartesian position                |
| Y                     | Y coordinate of current cartesian position                |
| Z                     | Z coordinate of current cartesian position                |


Following attributes are available only on Isara1 systems:

| attribute | description             |
|-----------|-------------------------|
| GonioX    | goniometer position (X) |
| GonioY    | goniometer position (Y) |
| GonioZ    | goniometer position (Z) |


# TODO
 - Log all
 - Plate Command
 - Hot puck for biomax
  - openlid/closelid rejected in biomax
  - Tool numbers`on commands? (chnage tool)
  - Wish: add timeout on time close diffractometer and FAULT--> and automatic recovery: abort, back, reset (BIOMAX)
  - review current biomax logic
