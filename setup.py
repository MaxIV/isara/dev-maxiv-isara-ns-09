#!/usr/bin/env python

###############################################################################
#     tangods-isara-ns-09
#
#     Copyright (C) 2021  MAX IV Laboratory, Lund Sweden.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
###############################################################################

from setuptools import (
    find_packages,
    setup,
)

setup(
    name="tangods-isara2",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="Tango device server for ISARA and ISARA2 sample changers.",
    author="KITS",
    author_email="kits-sw@maxiv.lu.se",
    license="GPL-3.0-or-later",
    url="https://gitlab.com/MaxIV/isara/dev-maxiv-isara2.git",
    entry_points={
        "console_scripts": [
            "Isara=isara2.dev.tangods:main",
        ]
    },
    packages=find_packages(exclude=("tests", "tests.*")),
    python_requires=">=3.11",
    install_requires=["pytango", "typing-extensions>=4.10.0"],
    extras_require={"tests": ["pytest", "pytest-forked"]},
)
