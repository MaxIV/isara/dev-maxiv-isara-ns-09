"""Emulator for ISARA1."""

from isara2.client.isara1 import Isara1ConnectionClient

from .base import (
    CommandName,
    InvalidToolNumber,
    IsaraBaseEmulator,
    Positions,
    encode,
    get_command_args,
    log,
)


class Isara1Emulator(IsaraBaseEmulator):
    """
    emulates ISARA robot, the first model, aka the blue robot at BioMAX
    """

    DEFAULT_TOOL_NAME = ""
    TOOL_MAPPING = Isara1ConnectionClient.TOOL_MAPPING

    def _handle_state_command(self) -> str:
        state = [
            encode(self._power_on),  # 0 = power
            encode(self._remote_mode),  # 1 = auto mode status
            "0",  # 2 = default status
            f"{self._current_tool_name}",  # 3 = tool name
            "",  # 4 = path name
            "",  # 5 = puck number of sample mounted on tool
            "",  # 6 = number of the sample on tool
            "",  # 7 = puck number of sample mounted on diffractometer
            "",  # 8 = number of sample mounted on diffractometer
            "",  # 9 = number of plate on tool
            "",  # 10 = not used
            "",  # 11 = barcode number
            encode(self._robot_arm.is_moving()),  # 12 = path running
            "0",  # 13 = LN2 regulation running Dewar
            "",  # 14 = not used
            f"{self._robot_arm.speed.ratio}",  # 15 = robot speed ratio (%)
            "1.16",  # 16 = Goniometer position (X)
            "1.17",  # 17 = Goniometer position (Y)
            "1.18",  # 18 = Goniometer position (Z)
            "",  # 19 = not used
            "",  # 20 = puck number of sample mounted on tool 2 (double gripper)
            "",  # 21 = number of the sample on tool 2 (double gripper)
            "",  # 22 = soak counter
        ]

        return f"state({','.join(state)})"

    def _handle_di_command(self) -> str:
        return "di(" + "0" * 99 + ")"

    def _handle_do_command(self) -> str:
        return "do(" + "0" * 99 + ")"

    def _handle_home_command(self, *args) -> str:
        tool_number = int(args[0])
        self._robot_arm.move_to(Positions.HOME)
        try:
            self.set_tool_by_number(tool_number)
        except InvalidToolNumber as exc:
            log(f"invalid tool number {tool_number=} {exc=}")
            return "Inconsistent parameters"
        return "home"

    def _handle_position_command(self) -> str:
        return "position(0.1,0.2,0.3,0.4,0.5,0.6)"

    def _handle_monitor_command(self, command: str) -> str:
        # handle ISARA1 specific monitor commands
        if command == CommandName.POSITION:
            return self._handle_position_command()

        return super()._handle_monitor_command(command)

    def _handle_operate_command(self, command: str) -> str:
        if command.startswith(CommandName.HOME):
            args = get_command_args(CommandName.HOME, command)
            return self._handle_home_command(*args)
        return super()._handle_operate_command(command)
