# ISARA emulator

Emulates network traffic of an ISARA sample changer robot.
Supports emulating ISARA2 systems and older ISARA system to a limited degree.
The emulator is used by the unit tests.

## Emulator API

To start the emulator, use `tests.emu.isara.start_emulator()` function.
The function starts emulator in a new thread and returns the emulator object.
Use `stop()` method on the emulator object to terminate the emulator thread.
See `isara()` and `isara2()` functions in `tests.fixtures` for examples.

### Emulator Object Methods

The emulator object returned by `start_emulator()` function provides methods to manipulate it's state.
These methods provide a way to simulate external action affecting the robot.
For example method `set_remote_mode()` allows to programmatically emulate turning the PLC key to _manul mode_ position.
Below are description of all available methods.

#### set_remote_mode()

Set robot's PLC to _remote mode_.
Simulates turning the PLC control key to _remote mode_ position.

#### set_manual_mode()

Set robot's PLC to _manual mode_.
Simulates turning the PLC control key to _manual mode_ position.

#### set_door_closed(is_closed: bool)

Sets robot's PLC hutch doors closed state.
When `is_closed` is `True`, simulates the states when the hutch is searched.
On `False`, simulates that the hutch is not searched.

#### Set tool by name

`set_tool_by_name(self, tool_name: str) -> None`

Set robot's tool by its name. Check `TOOL_MAPPING` for possible values.

#### Set tool by number

`set_tool_by_number(self, tool_number: int) -> None`

Set robot's tool by its number. Check `TOOL_MAPPING` for possible values.
