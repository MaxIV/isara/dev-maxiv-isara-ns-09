import asyncio
import sys
import traceback
from asyncio import (
    Event,
    IncompleteReadError,
    StreamReader,
    StreamWriter,
)
from enum import StrEnum
from queue import Queue

from .runner import Runner
from .watchable_attrs import WatchableAttrsMixin

PUCKS_NUM = 29

_logging_enabled = False


class CommandName(StrEnum):
    """Command names."""

    DI = "di"
    DO = "do"
    ON = "on"
    GET = "get"
    OFF = "off"
    PUT = "put"
    HOME = "home"
    TRAJ = "traj"
    ABORT = "abort"
    STATE = "state"
    RESET = "reset"
    MESSAGE = "message"
    OPENLID = "openlid"
    SPEEDUP = "speedup"
    CLOSELID = "closelid"
    POSITION = "position"
    SPEEDDOWN = "speeddown"
    CLEARMEMORY = "clearmemory"


def logging_enable(enable: bool) -> None:
    global _logging_enabled
    _logging_enabled = enable


def log(msg: str):
    if _logging_enabled:
        print(msg)
        sys.stdout.flush()


class InvalidToolName(Exception):
    """Tool name is invalid."""


class InvalidToolNumber(Exception):
    """Tool number is invalid."""


async def _read_command(connection_name: str, reader: StreamReader) -> str:
    cmd = await reader.readuntil(b"\r")
    log(f"{connection_name}> {cmd!r}")

    # chop off trailing \r and make a string
    return cmd[:-1].decode()


async def _write_reply(connection_name: str, writer: StreamWriter, reply: str):
    data = (reply + "\r").encode()
    writer.write(data)
    await writer.drain()

    log(f"{connection_name}< {data!r}")


def _encode_list(lst) -> str:
    encoded = [encode(v) for v in lst]
    return ",".join(encoded)


def encode(val):
    t = type(val)

    if t == bool:
        return "1" if val else "0"

    if t == list:
        return _encode_list(val)


def get_command_args(command_name: str, command: str) -> list:
    args_str = command[len(command_name) + 1 : -1]
    return args_str.split(",")


class Positions(StrEnum):
    """Position names."""

    HOME = "HOME"
    SOAK = "SOAK"


class _Speed:
    # TODO: check with the real robot for supported speed ratios
    RATIOS = ["0.01", "1.0", "10.0", "50.0", "75.0", "100.0"]

    def __init__(self):
        self._current_ration_idx = self._max_idx()

    def _max_idx(self):
        return len(self.RATIOS) - 1

    @property
    def ratio(self) -> str:
        return self.RATIOS[self._current_ration_idx]

    @property
    def decimal_ratio(self) -> float:
        """
        current speed ratio as decimal percentage number,
        that is a value between 0.0 and 1.0
        """
        return float(self.ratio) / 100.0

    def increase(self):
        if self._current_ration_idx == self._max_idx():
            # already at highest possible ratio
            return

        self._current_ration_idx += 1

    def decrease(self):
        if self._current_ration_idx == 0:
            # already at lowest possible speed ratio
            return

        self._current_ration_idx -= 1


class _RobotArm:
    def __init__(self):
        self._position = Positions.HOME
        self.speed = _Speed()

    def move_to(self, new_position: Positions):
        assert self._position is not None
        asyncio.create_task(self._run_trajectory(new_position))

    async def _run_trajectory(self, new_position: Positions):
        # start moving to new position
        log(f"moving to {new_position.value}")
        self._position = None

        # emulate that it take some time to reach destination position,
        # scale travel time according to current speed ratio
        travel_time = 0.5 / self.speed.decimal_ratio
        await asyncio.sleep(travel_time)

        # we have arrived at our new position
        self._position = new_position
        log(f"reached {new_position.value}")

    def is_moving(self) -> bool:
        return self._position is None

    def get_position(self) -> Positions:
        return self._position

    def get_position_name(self) -> str:
        if self._position is None:
            return "UNDEFINED"

        return self._position.value

    def change_speed(self, increase_speed: bool):
        if increase_speed:
            self.speed.increase()
        else:
            self.speed.decrease()


class _DewarLid:
    OPEN_POS = 10
    CLOSED_POS = 0

    def __init__(self):
        self._position = self.OPEN_POS
        self._target_position = None
        self._target_position_set = Event()

    def start(self):
        asyncio.create_task(self._run())

    def is_moving(self) -> bool:
        return self._target_position is not None

    async def _run(self):
        async def move_lid():
            while self._position != self._target_position:
                step = 1 if self._position < self._target_position else -1
                self._position += step
                await asyncio.sleep(0.6)

            self._target_position = None
            self._target_position_set.clear()

        while True:
            await self._target_position_set.wait()
            await move_lid()

    def open(self):
        self._target_position = self.OPEN_POS
        self._target_position_set.set()

    def close(self):
        self._target_position = self.CLOSED_POS
        self._target_position_set.set()


class IsaraBaseEmulator(Runner, WatchableAttrsMixin):
    """
    contains code shared by ISARA and ISARA2 emulation
    """

    DEFAULT_TOOL_NAME = "DefaultTool"
    TOOL_MAPPING = {0: DEFAULT_TOOL_NAME}

    def __init__(self, operate_port: int, monitor_port: int):
        super().__init__()

        # TCP ports to use
        self._operate_port = operate_port
        self._monitor_port = monitor_port

        self._message = "System OK for operation"
        #
        # emulates the robot PLC modes, i.e. the key switch modes
        #
        # we only emulate:
        #   'remote mode' (_remote_mode = True)
        #   'manual mode' (_remote_mode = False)
        #
        self._remote_mode = True
        self._door_closed = True
        self._power_on = True

        self._current_tool_name = self.DEFAULT_TOOL_NAME

        # puck present in the dewar
        self._dewar_pucks = [False] * PUCKS_NUM
        # start with a couple of pucks present, for convenience
        self._dewar_pucks[0] = True
        self._dewar_pucks[PUCKS_NUM - 1] = True

        self._robot_arm = _RobotArm()
        self._dewar_lid = _DewarLid()

    #
    # public API for changing the state of the ISARA
    #

    def set_remote_mode(self):
        self._remote_mode = True

    def set_manual_mode(self):
        self._remote_mode = False

    def set_door_closed(self, is_closed: bool):
        self._door_closed = is_closed

    def set_tool_by_name(self, tool_name: str) -> None:
        if tool_name in self.TOOL_MAPPING.values():
            self._current_tool_name = tool_name
        else:
            raise InvalidToolName

    def set_tool_by_number(self, tool_number: int) -> None:
        try:
            name = self.TOOL_MAPPING[tool_number]
        except KeyError as exc:
            raise InvalidToolNumber from exc
        else:
            self.set_tool_by_name(name)

    #
    # ISARA and ISARA2 common emulation code
    #

    def _handle_state_command(self):
        # needs model specific implementation
        raise NotImplementedError()

    def _handle_di_command(self):
        # needs model specific implementation
        raise NotImplementedError()

    def _handle_do_command(self):
        # needs model specific implementation
        raise NotImplementedError()

    def _check_plc(self) -> str | None:
        if not self._remote_mode:
            return "Remote mode requested"

        if not self._door_closed:
            return "Doors must be closed"

        return None

    def _handle_on_command(self) -> str:
        plc_err = self._check_plc()
        if plc_err is not None:
            return plc_err

        self._power_on = True
        return "on"

    def _handle_off_command(self) -> str:
        plc_err = self._check_plc()
        if plc_err is not None:
            return plc_err

        self._power_on = False
        return "off"

    def _handle_message_command(self) -> str:
        return "System OK for operation"

    def _handle_speed_command(self, command: str):
        plc_err = self._check_plc()
        if plc_err is not None:
            return plc_err

        self._robot_arm.change_speed(command == CommandName.SPEEDUP)

        return command

    def _handle_operate_command(self, command: str) -> str:
        if command == CommandName.ON:
            return self._handle_on_command()
        if command == CommandName.OFF:
            return self._handle_off_command()
        if command == CommandName.ABORT:
            return "abort"
        if command in [CommandName.SPEEDUP, CommandName.SPEEDDOWN]:
            return self._handle_speed_command(command)

        assert False, f"unexpected command {command} on operate connection"

    def _handle_monitor_command(self, command: str) -> str:
        if command == CommandName.STATE:
            return self._handle_state_command()
        if command == CommandName.DI:
            return self._handle_di_command()
        if command == CommandName.DO:
            return self._handle_do_command()
        if command == CommandName.MESSAGE:
            return self._handle_message_command()

        assert False, f"unexpected command {command} on monitor connection"

    async def _new_operate_connection(self, reader: StreamReader, writer: StreamWriter):
        log("new operate connection")
        try:
            while True:
                cmd = await _read_command("operate", reader)
                reply = self._handle_operate_command(cmd)

                await _write_reply("operate", writer, reply)
        except IncompleteReadError:
            # connection closed, we are done here
            return
        except:  # noqa
            print(traceback.format_exc())

    async def _new_monitor_connection(self, reader: StreamReader, writer: StreamWriter):
        log("new monitor connection")
        try:
            while True:
                cmd = await _read_command("monitor", reader)
                reply = self._handle_monitor_command(cmd)

                await _write_reply("monitor", writer, reply)
        except IncompleteReadError:
            # connection closed, we are done here
            return
        except:  # noqa
            # unexpected exception
            print(traceback.format_exc())

    async def _run(self, backchannel: Queue):
        self._dewar_lid.start()

        op_srv = await asyncio.start_server(
            self._new_operate_connection, host="0.0.0.0", port=self._operate_port
        )

        mon_srv = await asyncio.start_server(
            self._new_monitor_connection, host="0.0.0.0", port=self._monitor_port
        )

        asyncio.gather(
            op_srv.serve_forever(),
            mon_srv.serve_forever(),
        )

        loop = asyncio.get_running_loop()
        exit_event = asyncio.Event()
        backchannel.put((loop, exit_event))

        await exit_event.wait()
