import asyncio
from queue import Queue
from threading import Thread


class Runner:
    def start(self):
        backchannel = Queue()
        self._thread = Thread(target=lambda: asyncio.run(self._run(backchannel)))
        self._thread.start()

        #
        # wait until _run() is finished with setting up socket serving stuff,
        # _run() signals that it is ready by sending event loop and exit event objects,
        # which we can use for stopping the serving thread
        #
        self._loop, self._exit_event = backchannel.get()

    def stop(self):
        self._loop.call_soon_threadsafe(self._exit_event.set)
        self._thread.join()

    async def _run(self, backchannel: Queue):
        raise NotImplementedError()
