"""Emulator for ISARA2."""

from isara2.client.isara2 import Isara2ConnectionClient

from .base import (
    CommandName,
    InvalidToolNumber,
    IsaraBaseEmulator,
    Positions,
    encode,
    get_command_args,
    log,
)


class Isara2Emulator(IsaraBaseEmulator):
    """Emulator for ISARA robot model "ISARA2"."""

    DEFAULT_TOOL_NAME = ""
    TOOL_MAPPING = Isara2ConnectionClient.TOOL_MAPPING

    def _handle_state_command(self) -> str:
        power_on = encode(self._power_on)
        remote_mode = encode(self._remote_mode)
        position = self._robot_arm.get_position_name()
        path_running = encode(self._robot_arm.is_moving())
        speed_ratio = self._robot_arm.speed.ratio
        tool_name = self._current_tool_name

        return (
            f"state({power_on},{remote_mode},1,{tool_name},{position},,1,1,-1,-1,-1,-1,-1,"
            f"-1,-1,-1,,{path_running},0,{speed_ratio},0,0,0.3865678,75.0,72.0,1,0,0,"
            f"{self._message},67108864,152.9,-390.8,"
            "-17.3,-180.0,0.0,89.1,-75.6,-18.8,93.6,0.0,105.3,-165.5,,1,,1,0,0,0,0,"
            "0,0,0,0,0,0,0,0,0,changetool|3|3|0|-2.441|0.068|392.37|0.0|0.0|-0.984)"
        )

    def _handle_di_command(self) -> str:
        return (
            "di(0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,"
            "0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,"
            "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0)"
        )

    def _handle_do_command(self) -> str:
        puck_presence = encode(self._dewar_pucks)

        return (
            "do("
            "0,0,1,0,0,1,0,1,0,0,"  # 00 - 09
            "1,0,0,0,0,0,0,0,0,0,"  # 10 - 19
            "0,0,0,1,0,0,0,0,0,0,"  # 20 - 29
            "0,0,0,0,0,0,0,0,0,0,"  # 30 - 39
            "0,0,0,1,0,0,0,0,0,0,"  # 40 - 49
            f"1,0,0,0,0,0,{puck_presence},0,0,0,0,0,"  # 50 - 89
            "0,0,0,0,0,0,0,0,0,0,"  # 90 - 99
            "0,0,0,0,0,0,0,0,0,0,"  # 100 - 110
            "0,0)"
        )

    def _handle_get_traj(self) -> str:
        self._robot_arm.move_to(Positions.HOME)
        return "get"

    def _handle_put_traj(self) -> str:
        if self._robot_arm.get_position() != Positions.SOAK:
            return "Rejected - Trajectory must start at position: SOAK"

        return "put"

    def _handle_change_tool(self, *args) -> str:
        tool_number = int(args[0])
        self._robot_arm.move_to(Positions.HOME)
        try:
            self.set_tool_by_number(tool_number)
        except InvalidToolNumber as exc:
            log(f"invalid tool number {tool_number=} {exc=}")
            return "Inconsistent parameters"
        return "changetool"

    def _handle_traj_command(self, name, *args) -> str:
        if not self._power_on:
            return "Robot power disabled"

        if self._robot_arm.is_moving():
            return "Path already running"

        if self._dewar_lid.is_moving():
            return "Disabled when lid is moving"

        if name == "soak":
            self._robot_arm.move_to(Positions.SOAK)
            return "soak"

        if name == "home":
            self._robot_arm.move_to(Positions.HOME)
            return "home"

        if name == "back":
            return "back"

        if name == "get":
            return self._handle_get_traj()

        if name == "put":
            return self._handle_put_traj()

        if name == "changetool":
            return self._handle_change_tool(*args)

        raise NotImplementedError(f"running trajectory '{name}'")

    def _handle_openlid_command(self) -> str:
        self._dewar_lid.open()
        return "openlid"

    def _handle_closelid_command(self) -> str:
        self._dewar_lid.close()
        return "closelid"

    def _handle_clearmemory_command(self) -> str:
        if self._robot_arm.is_moving():
            return "Disabled when path is running"

        return "clearmemory"

    def _handle_reset_command(self) -> str:
        return "reset"

    def _handle_operate_command(self, command: str) -> str:
        #
        # handle ISARA2 specific operate commands
        #

        if command.startswith(CommandName.TRAJ):
            args = get_command_args(CommandName.TRAJ, command)
            return self._handle_traj_command(*args)

        if command == CommandName.OPENLID:
            return self._handle_openlid_command()

        if command == CommandName.CLOSELID:
            return self._handle_closelid_command()

        if command == CommandName.CLEARMEMORY:
            return self._handle_clearmemory_command()

        if command == CommandName.RESET:
            return self._handle_reset_command()

        # handle generic operate commands
        return super()._handle_operate_command(command)
