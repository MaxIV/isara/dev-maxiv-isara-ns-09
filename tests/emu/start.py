from .base import (
    log,
    logging_enable,
)
from .isara1 import Isara1Emulator
from .isara2 import Isara2Emulator


def start_emulator(
    model: str, operate_port: int, monitor_port: int, enable_logging=False
):
    logging_enable(enable_logging)

    def init_model():
        classes = {"ISARA1": Isara1Emulator, "ISARA2": Isara2Emulator}
        klass = classes[model]

        return klass(operate_port, monitor_port)

    isara = init_model()

    log(
        f"emulating {model} API\n"
        f" operate port: {operate_port}\n"
        f" monitor port: {monitor_port}"
    )

    isara.start()

    return isara
