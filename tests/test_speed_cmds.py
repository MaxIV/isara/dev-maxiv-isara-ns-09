from tests.utils import poll_attribute_until


def _test_speed_cmds(dev):
    """
    test that we can increase ana reduce speed ratio with
    'SpeedUp' and 'SpeedDown' commands
    """
    current_speed = dev.SpeedRatio

    # we assume that we start at max possible speed ratio
    assert current_speed == 100.0

    # check that reducing speed ratio works
    dev.SpeedDown()
    poll_attribute_until(dev, "SpeedRatio", lambda v: v < current_speed)

    # check that increasing speed ratio works
    current_speed = dev.SpeedRatio
    dev.SpeedUp()
    poll_attribute_until(dev, "SpeedRatio", lambda v: v > current_speed)


def test_isara1_speed_cmds(isara1, device):
    _test_speed_cmds(device)


def test_isara2_speed_cmds(isara2, device):
    _test_speed_cmds(device)
