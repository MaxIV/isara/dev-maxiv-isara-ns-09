"""Tests for Tango device status."""

import re
from time import sleep

from tests.utils import COMMAND_CACHE_TIMEOUT

ISARA1_STATUS = {
    "Barcode()",
    "SampleNumberOnDiff(-1)",
    "PuckNumberOnDiff(-1)",
    "SpeedRatio(100.0)",
    "RemoteMode(True)",
    "Powered(True)",
    "Tool()",
    "PlateNumberOnTool()",
    "LN2Regulation(False)",
    "PuckNumberOnToolB()",
    "PathRunning(False)",
    "SampleNumberOnToolB()",
    "FaultStatus(False)",
    "SampleNumberOnTool()",
    "Path()",
    "PathSafe(False)",
    "PuckNumberOnTool()",
    "Message(System OK for operation)",
    "CurrentNumberOfSoaking()",
}

ISARA2_STATUS = {
    "Powered(True)",
    "Tool()",
    "PositionName(HOME)",
    "Path()",
    "PathRunning(False)",
    "PathSafe(True)",
    "PuckNumberOnTool(-1)",
    "SampleNumberOnTool(-1)",
    "PuckNumberOnToolB(-1)",
    "SampleNumberOnToolB(-1)",
    "Barcode()",
    "PuckNumberOnDiff(-1)",
    "SampleNumberOnDiff(-1)",
    "PlateNumberOnTool(-1)",
    "LN2Regulation(False)",
    "RemoteMode(True)",
    "FaultStatus(True)",
    "SpeedRatio(100.0)",
    "CurrentNumberOfSoaking(0)",
    "Message(System OK for operation)",
}


def _parse_status(status: str) -> set[str]:
    def parts():
        pattern = re.compile(r"\w+\([\w\-\. ]*\)")
        for line in status.splitlines():
            match = pattern.match(line)
            if match:
                yield match.group()

    return set(parts())


def test_isara1_status(isara1, device):
    # invoke Status command
    status = _parse_status(device.Status())

    # check that we got expected status
    assert status == ISARA1_STATUS

    # change tool and check status again
    isara1.set_tool_by_name("Double")
    # We do not go through normal commands, so command cache does not get invalidated
    sleep(COMMAND_CACHE_TIMEOUT * 1.1)
    status = _parse_status(device.Status())
    assert "Tool()" not in status
    assert "Tool(Double)" in status


def test_isara2_status(isara2, device):
    # invoke Status command
    status = _parse_status(device.Status())

    # check that we got expected status
    assert status == ISARA2_STATUS

    # change tool and check status again
    isara2.set_tool_by_name("SingleGripper")
    # We do not go through normal commands, so command cache does not get invalidated
    sleep(COMMAND_CACHE_TIMEOUT * 1.1)
    status = _parse_status(device.Status())
    assert "Tool(DoubleGripper)" not in status
    assert "Tool(SingleGripper)" in status
