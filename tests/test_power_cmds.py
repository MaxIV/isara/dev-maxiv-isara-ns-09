from pytest import raises
from tango import DevFailed

from tests.utils import (
    get_isara_command_error,
    poll_attribute_until,
)


def _test_cycle_power(dev):
    # we assume that it's powered on initially
    assert dev.Powered

    # test powering off
    dev.PowerOff()
    poll_attribute_until(dev, "Powered", False)
    assert not dev.Powered

    # test powering on
    dev.PowerOn()
    poll_attribute_until(dev, "Powered", True)
    assert dev.Powered


def test_isara1_cycle_power(isara1, device):
    _test_cycle_power(device)


def test_isara2_cycle_power(isara2, device):
    _test_cycle_power(device)


def _test_manual_mode_power_on(emulator, dev):
    """
    test issuing 'power on' command while the robot is manual mode
    """

    # power off the robot
    dev.PowerOff()
    poll_attribute_until(dev, "Powered", False)

    # put the robot manual mode
    emulator.set_manual_mode()
    poll_attribute_until(dev, "RemoteMode", False)

    # try to power on the robot
    with raises(DevFailed) as excinfo:
        dev.PowerOn()

    # we should get an error message
    err_msg = get_isara_command_error(excinfo.value)
    assert err_msg == "Remote mode requested"

    # the robot should still be powered off
    assert not dev.Powered


def test_isara1_manual_mode_power_on(isara1, device):
    _test_manual_mode_power_on(isara1, device)


def test_isara2_manual_mode_power_on(isara2, device):
    _test_manual_mode_power_on(isara2, device)


def _test_door_opened_power_on(emulator, dev):
    """
    test issuing 'power on' command while the hutch door is open
    """

    # power off the robot
    dev.PowerOff()
    poll_attribute_until(dev, "Powered", False)

    # 'open' hutch door
    emulator.set_door_closed(False)

    # try to power on the robot
    with raises(DevFailed) as excinfo:
        dev.PowerOn()

    # we should get an error message
    err_msg = get_isara_command_error(excinfo.value)
    assert err_msg == "Doors must be closed"

    # the robot should still be powered off
    assert not dev.Powered


def test_isara1_door_opened_power_on(isara1, device):  # noqa F811
    _test_door_opened_power_on(isara1, device)


def test_isara2_door_opened_power_on(isara2, device):  # noqa F811
    _test_door_opened_power_on(isara2, device)
