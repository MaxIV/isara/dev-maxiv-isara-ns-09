import pytest
from tango.test_context import DeviceTestContext

from isara2.dev.tangods import ISARA
from tests.emu.start import start_emulator
from tests.utils import COMMAND_CACHE_TIMEOUT

OPERATE_PORT = 2222
MONITOR_PORT = 3333


def _start_emulator(model: str):
    return start_emulator(model, OPERATE_PORT, MONITOR_PORT, enable_logging=False)


@pytest.fixture
def isara1():
    emu = _start_emulator("ISARA1")

    yield emu

    emu.stop()


@pytest.fixture
def isara2():
    emu = _start_emulator("ISARA2")

    yield emu

    emu.stop()


@pytest.fixture
def isarax(request):
    """Indirection for parametrization of the Isara emulator fixtures.

    This combined with a decorator like below on the test function,
    allows to run the same test on multiple Isara implementations,
    where `isara1` and `isara2` are the names of the pytest fixture functions
    and `isarax` is the variable name to be used to access the current fixture
    from within the code of the test function:

        @pytest.mark.parametrize("isarax", ["isara1", "isara2"], indirect=True)
        def test_something(isarax):
            isarax.set_manual_mode()
    """
    return request.getfixturevalue(request.param)


@pytest.fixture
def device():
    properties = dict(
        host="localhost",
        operate_port=OPERATE_PORT,
        monitor_port=MONITOR_PORT,
        command_cache_timeout=COMMAND_CACHE_TIMEOUT,
    )
    with DeviceTestContext(ISARA, properties=properties, process=True) as dev:
        yield dev
