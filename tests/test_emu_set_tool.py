import pytest

import tests.emu.base


def test_set_tool_by_name_isara2(isara2):
    assert "" == isara2._current_tool_name

    isara2.set_tool_by_name("ToolChanger")
    assert "ToolChanger" == isara2._current_tool_name

    isara2.set_tool_by_number(1)
    assert "Cryotong" == isara2._current_tool_name

    with pytest.raises(tests.emu.base.InvalidToolName):
        isara2.set_tool_by_name("something")
    assert "Cryotong" == isara2._current_tool_name  # Check tool has not changed

    with pytest.raises(tests.emu.base.InvalidToolNumber):
        isara2.set_tool_by_number(777)
    assert "Cryotong" == isara2._current_tool_name  # Check tool has not changed


def test_set_tool_by_name_isara1(isara1):
    assert "" == isara1._current_tool_name

    isara1.set_tool_by_name("Flange")
    assert "Flange" == isara1._current_tool_name

    isara1.set_tool_by_number(1)
    assert "UnipuckGripper" == isara1._current_tool_name

    with pytest.raises(tests.emu.base.InvalidToolName):
        isara1.set_tool_by_name("something")
    assert "UnipuckGripper" == isara1._current_tool_name  # Check tool has not changed

    with pytest.raises(tests.emu.base.InvalidToolNumber):
        isara1.set_tool_by_number(777)
    assert "UnipuckGripper" == isara1._current_tool_name  # Check tool has not changed
