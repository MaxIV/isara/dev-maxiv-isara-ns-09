import pytest
import tango

from .utils import get_isara_command_error


@pytest.mark.parametrize("isarax", ["isara1", "isara2"], indirect=True)
def test_get_without_tool(isarax, device):
    with pytest.raises(tango.DevFailed) as dev_failed:
        device.Get()
    error_message = get_isara_command_error(dev_failed.value)
    assert "No tool on Isara robot" in error_message
