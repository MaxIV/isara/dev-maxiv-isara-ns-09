from pytest import (
    mark,
    raises,
)
from tango import DevFailed

from tests.utils import (
    get_isara_command_error,
    poll_attribute_until,
)


@mark.parametrize("isarax", ["isara1", "isara2"], indirect=True)
def test_put_without_tool(isarax, device):
    """PUT should fail when there is no tool."""
    with raises(DevFailed) as excinfo:
        device.Put(["1", "1"])
    error_message = get_isara_command_error(excinfo.value)
    assert "No tool on Isara robot" in error_message


def test_put_from_home(isara2, device):
    """
    for ISARA2 robot, test putting sample on gonio starting at 'HOME' position
    """
    isara2.set_tool_by_number(0)

    assert device.PositionName == "HOME"

    #
    # we should get an 'rejected' error
    #
    with raises(DevFailed) as excinfo:
        device.Put(["1", "1"])

    err_msg = get_isara_command_error(excinfo.value)
    assert err_msg == "Rejected - Trajectory must start at position: SOAK"


def test_put(isara2, device):
    """
    for ISARA2 robot, test moving to 'SOAK' position and puting a sample on gonio
    """
    isara2.set_tool_by_number(0)

    assert device.PositionName == "HOME"
    assert device.SpeedRatio == 100.0

    # goto 'SOAK' position
    device.Soak()
    poll_attribute_until(device, "PositionName", "SOAK")

    # put sample 1 from puck 1 on gonio
    device.Put(["1", "1"])
