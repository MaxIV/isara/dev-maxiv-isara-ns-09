import pytest
import tango

from tests.utils import (
    get_isara_command_error,
    poll_attribute_until,
)


def test_isara1_change_tool(isara1, device) -> None:
    status = device.Status()
    assert "Tool()" in status

    device.ChangeTool(1)
    poll_attribute_until(device, "PathRunning", False)
    status = device.Status()
    assert "Tool(UnipuckGripper)" in status

    with pytest.raises(tango.DevFailed) as exc:
        device.ChangeTool(777)
    poll_attribute_until(device, "PathRunning", False)
    assert get_isara_command_error(exc.value) == "Inconsistent parameters"
    assert "Tool(UnipuckGripper)" in status  # Check tool has not changed

    device.ChangeTool(2)
    poll_attribute_until(device, "PathRunning", False)
    status = device.Status()
    assert "Tool(UnipuckGripper)" not in status
    assert "Tool(RotatingGripper)" in status


def test_isara2_change_tool(isara2, device) -> None:
    status = device.Status()
    assert "Tool()" in status

    device.ChangeTool(2)
    poll_attribute_until(device, "PathRunning", False)
    status = device.Status()
    assert "Tool(SingleGripper)" in status

    with pytest.raises(tango.DevFailed) as exc:
        device.ChangeTool(777)
    poll_attribute_until(device, "PathRunning", False)
    assert get_isara_command_error(exc.value) == "Inconsistent parameters"
    assert "Tool(SingleGripper)" in status  # Check tool has not changed

    device.ChangeTool(6)
    poll_attribute_until(device, "PathRunning", False)
    status = device.Status()
    assert "Tool(SingleGripper)" not in status
    assert "Tool(PlateGripper)" in status
