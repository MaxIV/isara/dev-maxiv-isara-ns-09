from collections.abc import Callable
from time import sleep
from typing import Any

from tango import (
    DevFailed,
    DeviceProxy,
)

# use short timeout to speed up the tests,
# that are polling device attributes for changes
COMMAND_CACHE_TIMEOUT = 0.001


def get_isara_command_error(ex: DevFailed) -> str | None:
    #
    # The ISARA command errors are signaled by raising DevError exception,
    # with reason set to 'ISARACommandError' and 'desc' to the error message
    # from ISARA. The DevError will be wrapped inside DevFailed exception by
    # tango.
    #

    # Check if provided exception contains an DevError signalling ISARA command error.
    for err in ex.args:
        if err.reason == "ISARACommandError":
            return err.desc

    # this is not an ISARA command error exception
    return None


def poll_attribute_until(dev: DeviceProxy, attr_name: str, condition: Callable | Any):
    """
    Poll tango device attribute until it's value fulfills specified condition.

    The condition can be either a callable or some value.

    If condition is callable, it will be invoked with condition(attr_value) and polling
    will stop when true is returned.

    If condition is not callable, polling will stop when condition == attr_value is true.
    """

    def get_callable_condition():
        if callable(condition):
            # already callable
            return condition

        # wrap it
        return lambda val: val == condition

    def poll_attribute(attr_check: Callable):
        while True:
            val = dev.read_attribute(attr_name).value
            if attr_check(val):
                return

            sleep(COMMAND_CACHE_TIMEOUT / 2)

    poll_attribute(get_callable_condition())
